import java.util.ArrayList;
import java.util.Scanner;


public class Pig {
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		ArrayList<Player> players = initialize();
		takeTurn(players);
		
		System.out.println("The winners are: ");
		for (int i=0;i<getWinners(players).size();i++){
			System.out.println(getWinners(players).get(i).getName());
		}
	}
	
	//Asks the player for the information needed to play the game
	//Based on the information given it loops until the given number of players are added
	private static ArrayList<Player> initialize(){
		ArrayList<Player> players = new ArrayList<Player>();
		System.out.print("How many players will attend: ");
		int playing = Integer.parseInt(sc.nextLine());
			
		for (int i=0;i<playing;i++){
			System.out.print("Name of player " + (i+1) + ": ");
			players.add(i, new Player(sc.nextLine()));
			players.get(i).addDie(6);
		}
		
		return players;
	}
		
	private static void takeTurn(ArrayList<Player> players){
		int pot = 0;
		boolean switchPlayer, stop = false;
		
		//Starts the game
		while (!stop){
			for(int i=0;i<players.size();i++){
				System.out.println(players.get(i).getName() + "'s turn! You have " 
						+ players.get(i).getPoints() + " points now.");
				switchPlayer = false;
				
				//Lets the player currently playing play until 1 is rolled or EXIT is typed
				while (switchPlayer == false){
					players.get(i).rollDice();
				
					if(players.get(i).getDieValue() == 1){
						System.out.println("Ooops! Die = 1. Pot lost.");
						pot = 0;
						switchPlayer = true;
					}else{
						pot += players.get(i).getDieValue();
						System.out.println("Pot = " + pot + ". Press Enter to continue, " +
								"or write EXIT to take the pot.");
					}
					
					if ("EXIT".equals(sc.nextLine())){
						players.get(i).increaseScore(pot);
						pot = 0;
						switchPlayer = true;
					}
				}
				
			}
			
			//Checks if someone have more than 100 points
			for (int i=0;i<players.size();i++){
				if(players.get(i).getPoints() >= 100){
					stop = true;
				}
			}
		}
	}
	
	//Loops through all the players
	//If someone have more than highscore the list is cleared and they are added
	//If the player have the same as highscore the player is added to the list 
	private static ArrayList<Player> getWinners(ArrayList<Player> players){
		ArrayList<Player> winners = new ArrayList<Player>();
		int highscore = -1;
		
		for (int i=0;i<players.size();i++){
			if(highscore < players.get(i).getPoints()){
				winners.clear();
				highscore = players.get(i).getPoints();
				winners.add(new Player(players.get(i).getName()));
			}else if(highscore == players.get(i).getPoints()){
				winners.add(new Player(players.get(i).getName()));
			}
		}
		return winners;
	}
}
