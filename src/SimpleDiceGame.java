import java.util.ArrayList;
import java.util.Scanner;

//Christian Heina 940323-3274

public class SimpleDiceGame {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		ArrayList<Player> players = initialize();
		takeTurn(players);
		
		System.out.println("The winners are: ");
		for (int i=0;i<getWinners(players).size();i++){
			System.out.println(getWinners(players).get(i).getName());
		}
	}
	
	//Asks the player for the information needed to play the game
	//Based on the information given it loops until the given number of players are added
	private static ArrayList<Player> initialize(){
		ArrayList<Player> players = new ArrayList<Player>();
		System.out.print("How many players will attend: ");
		int playing = Integer.parseInt(sc.nextLine());
		System.out.print("How many dice per player: ");
		int dice = Integer.parseInt(sc.nextLine());
		System.out.print("How many sides per dice: ");
		int sides = Integer.parseInt(sc.nextLine());
		
		for (int i=0;i<playing;i++){
			System.out.print("Name of player " + (i+1) + ": ");
			players.add(i, new Player(sc.nextLine()));
		}
		for(int i=0;i<players.size();i++){
			for(int y=0;y<dice;y++){
				players.get(i).addDie(sides);
			}
		}
		return players;
	}
	
	//Loops through all players 5 times and let each of them make their guess and their roll
	private static void takeTurn(ArrayList<Player> players){
		int value;
		
		for (int i=0; i<5;i++){
			for (int y=0;y<players.size();y++) {
				System.out.print(players.get(y).getName() + " guess a value: ");
				value = Integer.parseInt(sc.nextLine());
				players.get(y).rollDice();
				System.out.println("Dice value is " + players.get(y).getDieValue());
				if(value == players.get(y).getDieValue()){
					System.out.println("Correct guess!");
					players.get(y).increaseScore();
				}
			}
		}
	}
	
	//Loops through all the players
	//If someone have more than highscore the list is cleared and they are added
	//If the player have the same as highscore the player is added to the list 
	private static ArrayList<Player> getWinners(ArrayList<Player> players){
		ArrayList<Player> winners = new ArrayList<Player>();
		int highscore = -1;
		
		for (int i=0;i<players.size();i++){
			if(highscore < players.get(i).getPoints()){
				winners.clear();
				highscore = players.get(i).getPoints();
				winners.add(new Player(players.get(i).getName()));
			}else if(highscore == players.get(i).getPoints()){
				winners.add(new Player(players.get(i).getName()));
			}
		}
		return winners;
	}
}
