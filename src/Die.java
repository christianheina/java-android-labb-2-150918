import java.util.Random;


public class Die {
	private int sides, currentValue;
	private static Random rand = new Random();
	
	public Die(int dieSides){
		sides = dieSides;
	}
	
	public void roll(){
		currentValue = rand.nextInt(sides) + 1;
	}
	
	public int getCurrentValue(){
		return currentValue;
	}
	
	public int getDieSides(){
		return sides;
	}
}
