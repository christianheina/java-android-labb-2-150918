import java.util.ArrayList;


public class Player {
	private String playerName;
	private int points = 0;
	private ArrayList<Die> dice = new ArrayList<Die>();
	
	public Player (String name) {
		playerName = name;
	}
	
	public void rollDice(){
		for (int i=0;i<dice.size();i++){
			dice.get(i).roll();
		}
	}
	
	public int getDieValue(){
		int sum = 0;
		for (int i=0;i<dice.size();i++){
			sum += dice.get(i).getCurrentValue();
		}
		return sum;
	}
	
	public void increaseScore(){
		points++;
	}
	
	public void increaseScore(int increaseBy){
		points += increaseBy;
	}
	
	public void addDie(int sides){
		dice.add(new Die(sides));
	}
	
	public String getName(){
		return playerName;
	}
	
	public int getPoints(){
		return points;
	}
}
